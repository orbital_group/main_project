//
// NIST-developed software is provided by NIST as a public service.
// You may use, copy and distribute copies of the software in any medium,
// provided that you keep intact this entire notice. You may improve, 
// modify and create derivative works of the software or any portion of
// the software, and you may copy and distribute such modifications or
// works. Modified works should carry a notice stating that you changed
// the software and should note the date and nature of any such change.
// Please explicitly acknowledge the National Institute of Standards and 
// Technology as the source of the software.
//
// NIST-developed software is expressly provided "AS IS." NIST MAKES NO 
// WARRANTY OF ANY KIND, EXPRESS, IMPLIED, IN FACT OR ARISING BY OPERATION
// OF LAW, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTY OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT AND DATA ACCURACY. NIST
// NEITHER REPRESENTS NOR WARRANTS THAT THE OPERATION OF THE SOFTWARE WILL BE 
// UNINTERRUPTED OR ERROR-FREE, OR THAT ANY DEFECTS WILL BE CORRECTED. NIST 
// DOES NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OF THE SOFTWARE
// OR THE RESULTS THEREOF, INCLUDING BUT NOT LIMITED TO THE CORRECTNESS, ACCURACY,
// RELIABILITY, OR USEFULNESS OF THE SOFTWARE.
//
// You are solely responsible for determining the appropriateness of using and 
// distributing the software and you assume all risks associated with its use, 
// including but not limited to the risks and costs of program errors, compliance 
// with applicable laws, damage to or loss of data, programs or equipment, and 
// the unavailability or interruption of operation. This software is not intended
// to be used in any situation where a failure could cause risk of injury or 
// damage to property. The software developed by NIST employees is not subject to
// copyright protection within the United States.
//

// disable deprecation for sprintf and fopen

#include <stdio.h>
#include "xil_printf.h"
#include "xparameters.h"
#include "xtmrctr.h"
#include <stdlib.h>

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "crypto_hash.h"
#include "api.h"

#define TMRCTR_DEVICE_ID	XPAR_TMRCTR_0_DEVICE_ID
XTmrCtr TimerCounter; /* The instance of the Tmrctr Device */
#define TIMER_COUNTER_0	 0

#define KAT_SUCCESS          0
#define KAT_FILE_OPEN_ERROR -1
#define KAT_DATA_ERROR      -3
#define KAT_CRYPTO_FAILURE  -4

#define MAX_FILE_NAME				256
#define MAX_MESSAGE_LENGTH			256

void init_buffer(unsigned char *buffer, unsigned long long numbytes);

void write_buffer(unsigned char* buffer, unsigned long long numbytes, unsigned char text[]);

void fprint_bstr(FILE *fp, const char *label, const unsigned char *data, unsigned long long length);

int generate_test_vectors();

int main()
{

	int ret = generate_test_vectors();

	return ret;
}

int generate_test_vectors()
{
	unsigned char       msg[MAX_MESSAGE_LENGTH];
	unsigned char		digest[CRYPTO_BYTES];
	int                 ret_val = KAT_SUCCESS;
	XTmrCtr *TmrCtrInstancePtr = &TimerCounter;
	int TmrCtrNumber = TIMER_COUNTER_0;
	int Status;
	int DeviceId = TMRCTR_DEVICE_ID;
	double ResetValue = 0;
	double Execution_time = 0;
	int time = 0;
	int count = 0;
	/*
	 * Initialize the timer counter so that it's ready to use,
	 * specify the device ID that is generated in xparameters.h
	 */
	Status = XTmrCtr_Initialize(TmrCtrInstancePtr, DeviceId);

	init_buffer(msg, MAX_MESSAGE_LENGTH);
//	XTmrCtr_Start(TmrCtrInstancePtr, TmrCtrNumber);
//	int Start = XTmrCtr_GetValue(TmrCtrInstancePtr, TmrCtrNumber);
//	printf("Encryption Started...\n");

	for (unsigned long long mlen = 0; mlen <= MAX_MESSAGE_LENGTH; mlen++) {
//		count++;
//		printf("\nCount: %i\n", count);
//
//		printf("Message = ");
//		for(int i = 0; i < mlen; i++)
//		{
//			printf("%u", msg[i]);
//		}
//		printf("\n");
//
//		printf("Start Encrypting %i bytes of data --> ", count);
		XTmrCtr_Start(TmrCtrInstancePtr, TmrCtrNumber);		//start timer
	    ret_val = crypto_hash(digest, msg, mlen);			//start encryption
	    XTmrCtr_Stop(TmrCtrInstancePtr, TmrCtrNumber);		//stop timer
//	    ResetValue = XTmrCtr_GetValue(TmrCtrInstancePtr, TmrCtrNumber);	//store timer value after stop
//	    XTmrCtr_SetResetValue(TmrCtrInstancePtr, TmrCtrNumber, ResetValue);	//set that value to what gets reset when timer is restarted effectively accumulating the execution time
	    time = XTmrCtr_GetValue(TmrCtrInstancePtr, TmrCtrNumber);
//	    printf("%i cycles to encrypt\n", time);
	    printf("%i\n", time);
//	    Execution_time = Execution_time + time;

	    if(ret_val != 0) {
	    	print("crypto_hash failed\n");
	    	ret_val = KAT_CRYPTO_FAILURE;
	    }
//
//	    printf(" --> End of Encryption\n");
//
//	    printf("Encrypted output= ");
//		for(int i = 0; i < CRYPTO_BYTES; i++)
//			printf("%u", digest[i]);
//		printf("\n");
	}

//	Execution_time = XTmrCtr_GetValue(TmrCtrInstancePtr, TmrCtrNumber);
//	printf("\nCycles to execute all %i encryptions: %f\n", count, Execution_time);


	///////New Encryption Usage
//	unsigned char text[MAX_MESSAGE_LENGTH] = "Mark";

//	printf("Text input = ");

//	for(int i = 0; i < MAX_MESSAGE_LENGTH; i++)
//	{
//		printf("%u\n", text[i]);
//	}
//
//	write_buffer(msg, MAX_MESSAGE_LENGTH,text);
//
//	printf("Start Encrypting %i bytes of data --> ", count);
//	XTmrCtr_Start(TmrCtrInstancePtr, TmrCtrNumber);		//start timer
//  ret_val = crypto_hash(digest, msg, mlen);			//start encryption
//  XTmrCtr_Stop(TmrCtrInstancePtr, TmrCtrNumber);		//stop timer
//	ResetValue = XTmrCtr_GetValue(TmrCtrInstancePtr, TmrCtrNumber);	//store timer value after stop
//	XTmrCtr_SetResetValue(TmrCtrInstancePtr, TmrCtrNumber, ResetValue);	//set that value to what gets reset when timer is restarted effectively accumulating the execution time
//  time = XTmrCtr_GetValue(TmrCtrInstancePtr, TmrCtrNumber);
//  printf("%i cycles to encrypt\n", time);
//  Execution_time = Execution_time + time;
//	if(ret_val != 0) {
//		print("crypto_hash failed\n");
//		ret_val = KAT_CRYPTO_FAILURE;
//	}
//  printf(" --> End of Encryption\n");
//
//	if(ret_val != 0) {
//		print("crypto_hash failed\n");
//		ret_val = KAT_CRYPTO_FAILURE;
//	}
//
//	printf("Encrypted output= ");
//	for(int i = 0; i < CRYPTO_BYTES; i++)
//			print("%u", digest[i]);

	return ret_val;
}


void init_buffer(unsigned char *buffer, unsigned long long numbytes)
{
	for (unsigned long long i = 0; i < numbytes; i++)
		//buffer[i] = (unsigned char)i%10;
		buffer[i] = (unsigned char)rand()%10;
}

void write_buffer(unsigned char* buffer, unsigned long long numbytes, unsigned char text[])
{
	for (unsigned long long i = 0; i < numbytes; i++)
		buffer[i] = text[i];
}
